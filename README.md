# Go Gadget

This project is a collection of go cli applications built while reading [Powerful Command-Line Applications in Go
Build Fast and Maintainable Tools](https://pragprog.com/titles/rggo/powerful-command-line-applications-in-go/).

# Getting Started 

- Check what version of Go are you using: `go version`
- Check what operating system and processor architecture are you using:  `go env`
    - Make sure that `GO111MODULE=on` is set to use [Go Modules](https://github.com/golang/go/wiki/modules)

# Chapter 1 - Word Counter

Using git bash, create a new folder called wc and change to use it, then create a Go Module file:

## Windows

``` bash
$ mkdir -p /c/_code/carrie.dev/go-gadget/wc

$ cd /c/_code/carrie.dev/go-gadget/wc

$ go mod init carrie.dev/go-gadget/wc
go: creating new go.mod: module carrie.dev/go-gadget/wc
```

## Mac

``` bash
$ mkdir -p $HOME/carrie.dev/go-gadget/wc

$ cd $HOME/carrie.dev/go-gadget/wc
```

## Go Test

To execute the test, use the go test tool like this:

### Windows

``` bash
$ ls
go.mod main.go main_test.go

$ go test -v
=== RUN   TestCountWords
--- PASS: TestCountWords (0.00s)
PASS
ok      carrie.dev/go-gadget/wc 0.184s
```

## Go Build

The test passes, so you can compile the program with go build which creates the executable in the current directory:

``` bash
$ go build

$ ls
go.mod  main.go  main_test.go  wc.exe*
```

Tes the program out by passing it an input string:


``` bash
$ echo "My first command line tool with Go" | ./wc
7
```