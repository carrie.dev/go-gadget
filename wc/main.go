package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

/* For this tool, you import the bufio package to read text,
the fmt package to print formatted output,
the io package which provides the io.Reader interface,
and the os package so you can use operating system resources. */

func main() {
	// Calling the count function to count the number of words
	// received from the Standard Input and printing it out
	fmt.Println(count(os.Stdin))
}

func count(r io.Reader) int {
	// A scanner is used to read text from a Reader (such as files)
	scanner := bufio.NewScanner(r)

	// Define the scanner split type to words (default is split by lines)
	scanner.Split(bufio.ScanWords)

	// Defining a counter
	wc := 0

	// For every word scanned, increment the counter
	for scanner.Scan() {
		wc++
	}

	// Return the total
	return wc
}
